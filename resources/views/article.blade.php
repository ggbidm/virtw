@extends('layouts.main')

@section('meta_tags')

    <title>Virtual worlds - {{ $article->meta_title }}</title>

    <meta property="og:description" content="Virtual worlds - {{ $article->meta_descriptions }}" />
    <meta property="og:title" content="Virtual worlds - {{ ($article->vk_title) ? $article->vk_title : $article->meta_title }}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="ru-ru" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Virtual worlds')}}" />

    @if($article->meta_keywords)
    <meta name="keywords" content="{{ $article->meta_keywords }}">
    @endif

    @if($article->meta_descriptions)
    <meta name="description" content="{{ $article->meta_descriptions }}">
    @endif

    @if($article->image_url)
    <meta property="og:image" content="https://virtw.ru{{ $article->image_url }}" />
    @else
    <meta property="og:image" content="https://virtw.ru/images/og-logo.jpg" />
    @endif

@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-xs-12">
                            <h1 style="color: #656567;">{{ $article->title }}</h1>
                        </div>
{{--                        <div class="col col-lg-1 col-xs-12 pull-right">
                            <div class="float-right badge badge-sm badge-pill badge-outline-default"><i class="icon-eye"></i> {{ $article->views }}</div>
                        </div>--}}
                    </div>
                    <div class="row custom-a" style="margin-bottom: 25px; margin-top: 5px;">
                        <div class="col col-sm-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb" style="padding: 0; margin: 0; ">
                                    <li class="breadcrumb-item"><a title="На главную" href="/">Главная</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        <a title="{{ $article->category->title }}" href="{{ route('category', ['slug' => $article->category->slug]) }}">{{ $article->category->title }}</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                    @if($article->image_url)
                        <a title="{{ $article->title }}" href="{{ $article->image_url }}">
                            <img alt="{{ $article->title }}" style="margin:10px 0 10px 0; width: 100%;" src="{{ $article->image_url }}"/>
                        </a>
                    @endif


                    <div style="">
                        {!! $article->text !!}
                    </div>
                    @if($article->source_title && $article->source_url)<span class="text-muted">Источник: <a target="_blank" title='Источник статьи "{{ $article->title }}"' href="{{ $article->source_url }}">{{ $article->source_title }}</a></span><br/>@endif
                    {!! $article->showCreatedDate() !!}
                    <div style="float: right;">
                        <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                        <script src="https://yastatic.net/share2/share.js"></script>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter,telegram"></div>
                    </div>
                    <div class="clearfix"></div>

                    @include('comments', ['model' => $article])
                    <div class="clearfix"></div>

                    @include('partials.banner-yandex')

                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-sm-12 align-middle">
                            {{ $article->getPrevLink() }}
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-12 text-right align-middle">
                            {{ $article->getNextLink() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
