@extends('layouts.main')

@section('meta_tags')

    <title>Virtual worlds - Результат поиска</title>

    <meta property="og:description" content="Virtual worlds - Результат поиска" />
    <meta property="og:title" content="Virtual worlds - Результат поиска" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="ru-ru" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Virtual worlds')}}" />
    <meta property="og:image" content="https://virtw.ru/images/og-logo.jpg" />
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-custom">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>

                    @if($message)
                        <li>Ошибка поиска: {{ $message }}</li>
                    @else
                        <li class="breadcrumb-item">Результат поиска по "{{ $query }}" (результатов {{ $articles->count() }})</li>
                    @endif
                </ol>
            </nav>
        </div>
    </div>

    @foreach($articles as $article)
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row custom-a">
                            <div class="col col-sm-11">
                                <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}">
                                    <h4 style="font-size: 1.6rem; margin-bottom: 0; color: #656567;" class="card-title">{{ $article->title }}</h4>
                                </a>
                            </div>
                            <div class="col col-sm-1 pull-right">
                                <div style="color: #cec9c9;" class="float-right badge badge-sm badge-pill badge-outline-default"><i class="icon-eye"></i> {{ $article->views }}</div>
                            </div>
                        </div>
                        <div class="row custom-a" style="margin-bottom: 25px; margin-top: 5px;">
                            <div class="col col-sm-12">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb" style="padding: 0; margin: 0; ">
                                        <li class="breadcrumb-item"><a title="На главную" href="/">Главная</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">
                                            <a title="{{ $article->category->title }}" href="{{ route('category', ['slug' => $article->category->slug]) }}">{{ $article->category->title }}</a>
                                        </li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                        @if($article->image_url)
                            <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}">
                                <img alt="{{ $article->title }}" style="width: 100%;" src="{{ $article->image_url }}"/>
                            </a>
                        @endif

                        <div style="margin-top: 5px;/*text-align: justify;*/">
                            {!! $article->anons !!}
                        </div>

                        {!! $article->showCreatedDate() !!}

                        <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}" class="btn btn-outline-primary float-right" style="margin-top: 10px;">
                            Читать далее
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
