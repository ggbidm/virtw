@if ($paginator->hasPages())
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <ul class="pagination rounded-separated pagination-custom justify-content-center" style="margin: 0; padding: 0;">
                        @if ($paginator->onFirstPage())

                            <li class="page-item disabled">
                                <a class="page-link" href="#"><i class="icon-arrow-left"></i></a>
                            </li>

                        @else

                            <li class="page-item">
                                <a class="page-link" href="{{ $paginator->previousPageUrl() }}"><i class="icon-arrow-left"></i></a>
                            </li>

                        @endif

                        @foreach ($elements as $element)
                            @if (is_string($element))
                                <li class="page-item disabled">{{ $element }}</li>

                            @endif
                            @if (is_array($element))
                                @foreach ($element as $page => $url)
                                    @if ($page == $paginator->currentPage())

                                        <li class="page-item active"><a class="page-link" href="#">{{ $page }}</a></li>

                                    @else
                                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>

                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        @if ($paginator->hasMorePages())

                            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}"><i class="icon-arrow-right"></i></a></li>
                        @else

                            <li class="page-item disabled"><a class="page-link" href="#"><i class="icon-arrow-right"></i></a></li>

                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endif