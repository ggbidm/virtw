<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{ route('index') }}/</loc>
        <lastmod>{{ $indexUpdateDate->tz('GMT')->toAtomString() }}</lastmod>
        <priority>1</priority>
    </url>

    @php
        $pages = ceil($articlesPagination->total() / $articlesPagination->perpage());
    @endphp

    @if ($pages > 1)
        @for($page=2; $page<=$pages; $page++)
            <url>
                <loc>{{ route('index', ['page' => $page]) }}</loc>
                    <lastmod>{{ $indexUpdateDate->tz('GMT')->toAtomString() }}</lastmod>
                    <priority>1.0</priority>
            </url>
        @endfor
    @endif

    <url>
        <loc>https://virtw.ru/o-nas</loc>
        <lastmod>2019-09-19T20:08:56+01:00</lastmod>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>https://virtw.ru/login</loc>
        <lastmod>2019-09-19T20:08:56+01:00</lastmod>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>https://virtw.ru/password/reset</loc>
        <lastmod>2019-09-19T20:09:03+01:00</lastmod>
        <priority>0.8</priority>
    </url>

    @foreach ($categories as $category)
        <url>
            <loc>{{ url($category->slug) }}</loc>
            <lastmod>{{ $category->lastDateUpdate()->tz('GMT')->toAtomString() }}</lastmod>
            <priority>1</priority>
        </url>

        @php
            $pages = ceil($category->articles()->paginate()->total() / $category->articles()->paginate()->perpage());
        @endphp

        @if ($pages > 1)
            @for($page=2; $page<=$pages; $page++)
                <url>
                    <loc>{{ route('category', ['id' => $category->slug, 'page' => $page]) }}</loc>
                    <lastmod>{{ $category->lastDateUpdate()->tz('GMT')->toAtomString() }}</lastmod>
                    <priority>1.0</priority>
                </url>
            @endfor
        @endif

        @foreach ($category->articles as $article)
            <url>
                <loc>{{ route('article', ['category' => $category->slug, 'articles' => $article->slug]) }}</loc>
                <lastmod>{{ $article->updated_at->tz('GMT')->toAtomString() }}</lastmod>
                <priority>1</priority>
            </url>
        @endforeach
    @endforeach
</urlset>