<hr/>
<div>

    <div class="container">
        <div class="row">
            <a name="comments"><h2>Комментарии |{{ $model->comments->count() }}|</h2></a>
        </div>

        @foreach($model->comments as $comment)
            <div class="">
                <b>{{ $comment->user->name }} <small>30.10.2017 12:13</small></b></br>
                <p>{{ $comment->body }}</p>
            </div>
        @endforeach
        @guest
            <hr>
            <div>
                К сожалению, оставлять комментарии могут только авторизованные пользователи.<br>
                Пожалуйста, <a href="{{ route('login') }}">авторизуйтесь</a>.
                <br/>
                @include('auth/social')
            </div>
        @endguest

        @auth
            @if(Auth::user()->isActive())
            <hr>
                <form method="post" class="forms-sample" action="{{ route('comment.add') }}">
                    <input type="hidden" name="model_id" value="{{ $model->id }}"/>
                    {{ csrf_field() }}
                    {{--<div class="form-group">
                        --}}{{--<label for="comment_name">Имя</label>--}}{{--
                        <input type="text" class="form-control" id="comment_name" placeholder="Имя">
                    </div>--}}
                    <div class="form-group">
                        {{--<label for="comment_text">Сообщение</label>--}}
                        <textarea name="body" rows="4" class="form-control" id="comment_text" placeholder="Текст сообщения"></textarea>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Комментировать</button>
                </form>
            @endif
        @endauth
    </div>
</div>
