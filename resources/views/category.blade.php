@extends('layouts.main')

@section('meta_tags')

    @php
        $title = 'Virtual worlds - '.$category->meta_title;
        $description = 'Virtual worlds - '.$category->meta_descriptions;
        if ($page = Request::get('page')) {
            $title .= ', стр. '.$page;
            $description .= ', стр. '.$page;
        }
    @endphp
    <title>{{$title}}</title>
    <meta name="description" content="{{$description}}">
    <meta name="keywords" content="{{$category->meta_keywords}}">

    <meta property="og:description" content="Virtual worlds - {{ $category->meta_descriptions }}" />
    <meta property="og:title" content="Virtual worlds - {{ $category->title }}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="ru-ru" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Virtual worlds')}}" />
    <meta property="og:image" content="https://virtw.ru/images/og-logo.jpg" />
@endsection

@section('content')

    @include('partials.list')

@endsection

@section('custom-css')
    <style>
        .badge-comment {
            font-size: 12pt !Important;
            color: #070d4c5e !Important
        }
    </style>
@endsection
