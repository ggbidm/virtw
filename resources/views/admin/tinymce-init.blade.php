@include('mceImageUpload::upload_form', ['upload_url' => '/admin/upload-image'])

{{--<script src="{{ asset('//cdn.tinymce.com/4/tinymce.min.js') }}"></script>--}}
<script src="{{ asset('/vendors/tinymce/tinymce.js') }}"></script>

<script>
    $().ready(function () {
        tinyMCE.PluginManager.add('headers', function(editor, url) {
            ['h1', 'h2', 'h3', 'h4', 'mark'].forEach(function(name){
                editor.addButton("style-" + name, {
                    tooltip: "Toggle " + name,
                    text: name.toUpperCase(),
                    onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                    onPostRender: function() {
                        var self = this, setup = function() {
                            editor.formatter.formatChanged(name, function(state) {
                                self.active(state);
                            });
                        };
                        editor.formatter ? setup() : editor.on('init', setup);
                    }
                })
            });
        });

        tinymce.init({
            selector: '.tinymce',
            height: 600,
            theme: 'modern',
            language: "ru",
            language_url : '/vendors/tinymce/ru.js',
            plugins: [
                'headers spellchecker image code print preview searchreplace autolink directionality visualblocks visualchars fullscreen link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount  contextmenu colorpicker textpattern'
            ],
            toolbar1: 'undo redo | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | preview code fullscreen',
            toolbar2: 'image | link | style-h1 style-h2 style-h3 style-h4 style-mark',
            relative_urls: false,
            image_title: true,
            file_picker_types: 'image',
            file_browser_callback: function(field_name, url, type, win) {
                if (type == 'image') {
                    $('#formUpload input').click();
                }
            },
            init_instance_callback: function (editor) {
                editor.on('BeforeSetContent', function (e) {
                    const html = e.content;
                    const regex = /<img src="(.*?)".*?>/g;
                    const regex2 = /alt="([^"]*)"/g;
                    var m;
                    //console.log(e);

                    while ((m = regex.exec(html)) !== null) {
                        if (m.index === regex.lastIndex) {
                            regex.lastIndex++;
                        }
                        var m2 = regex2.exec(html)
                        console.log(m2[1]);

                        e.content = '<div><a title="'+m2[1]+'" href="'+ m[1].replace('medium', 'original') +'"><img style="margin: 5px 5px 5px 0;" alt="'+m2[1]+'" class="img-fluid" src="'+ m[1] +'" id="__mcenew"/></a><br><b>'+m2[1]+'</b></div>';
                    }
                });
            }
        });

    });
</script>