<header class="section-header">
    <div class="tbl">
        <div class="tbl-row">
            <div class="tbl-cell">
                <h3>{{ $title }}</h3>
                <ol class="breadcrumb breadcrumb-simple">
                    <li><a href="/admin">Главная</a></li>
                    @foreach($items as $i=>$item)
                        @if($i==count($items)-1)
                            <li class="active">{{ $item['title'] }}</li>
                        @else
                            <li><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                        @endif
                    @endforeach
                </ol>
            </div>
            @if(isset($actions) && count($actions) > 0)
                <div class="tbl-cell tbl-cell-action button" style="text-align: right;">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default-outline" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="font-icon glyphicon glyphicon-option-vertical"></i>
                        </button>
                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                            @foreach($actions as $i=>$item)
                                <a @if(isset($item['atrs'])) {!! $item['atrs'] !!} @endif class="dropdown-item" href="{{ $item['url'] }}">{{ $item['title'] }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</header>