@extends('layouts.admin')

@section('title', 'Редактирование категории')

@section('content')

    @include('admin.messages')

    @include('admin.categories.form', [
        'model' => $model
    ])

@endsection

@section('custom-js')
    @include('admin.categories.form-validation')
@endsection
