{!! Form::model($model, [
    'method' => ($model->id) ? 'PUT' : 'STORE',
    'route' => ($model->id) ? ['admin.categories.update', $model->id] : 'admin.categories.store',
    'style' => 'display:inline',
    'id' => 'model_form'
]) !!}

    <div class="form-group">
        {!! Form::label('title', 'Название') !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'id' => 'title'
        ]) !!}
    </div>

    @if ($model->id)
    <div class="form-group">
        {!! Form::label('slug', 'Slug') !!}
        {!! Form::text('slug', null, [
            'class' => 'form-control',
            'style' => 'disabled: disabled;',
            'disabled' => 'disabled'
        ]) !!}
    </div>
    @endif

    <div class="form-group">
        {!! Form::label('meta_title', 'Мета-заголовок') !!}
        {!! Form::text('meta_title', null, [
            'class' => 'form-control',
        ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('meta_keywords', 'Ключевые слова') !!}
        {!! Form::text('meta_keywords', null, [
            'class' => 'form-control',
        ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('meta_descriptions', 'Мета-описание') !!}
        {!! Form::text('meta_descriptions', null, [
            'class' => 'form-control',
        ]) !!}
    </div>

    <div class="form-group">
        {!! Form::submit(($model->id) ? 'Сохранить' : 'Создать', ['class' => 'btn btn-success']) !!}
    </div>

{!! Form::close() !!}