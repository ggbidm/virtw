@extends('layouts.admin')

@section('title', 'Категории')

@section('content')

    @include('admin.title', [
        'title' => 'Категории',
        'items' => [
            ['title' => 'Список']
        ],
        'actions' => [
            [
                'url' => route('admin.categories.create'),
                'title' => 'Добавить'
            ]
        ]
    ])
    @include('admin.messages')


    <table id="data_table" class="table">
        <thead>
        <tr>
            <th style="width: 50px;">ID</th>
            <th>Название</th>
            <th>Slug</th>
            <th style="width: 100px;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->title}}</td>
                <td>{{$category->slug}}</td>
                <td>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['admin.categories.destroy', $category->id],
                        'style' => 'display:inline',
                        'onsubmit' => 'return confirm(\'Подтверждаете удаление?\')'
                    ]) !!}
                    <div class="btn-group btn-group-lg" role="group">
                        <a href="{{route('admin.categories.edit', $category->id)}}" class="btn btn-primary">Редактировать</a>
                        {!! Form::submit('Удалить', ['class' => 'btn btn-warning']) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('custom-js')
    <script src="{{ asset('js/data-table.js') }}"></script>
@endsection