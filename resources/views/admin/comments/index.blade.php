@extends('layouts.admin')

@section('content')

    @include('admin.title', [
        'title' => 'Комментарии',
        'items' => [
            ['title' => 'Комментарии']
        ],
        'actions' => [
            /*[
                'url' => '#',
                'title' => 'Добавить задачу',
                'atrs' => 'data-toggle="modal" data-target="#add_task_modal"'
            ]*/
        ]
    ])

    @include('admin.messages')

    <table id="data_table" class="table">
        <thead>
        <tr>
            <th width="50%">Сообщение</th>
            <th width="30%">Статья</th>
            <th width="10%">Автор</th>
            <th width="10%">Добавлено</th>
            <th style="width: 100px;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>
                    {{$comment->body}}
                </td>
                <td>
                    {{ $comment->article->title }}
                </td>
                <td>
                    <a href="{{ route('admin.users.edit', ['user' => $comment->user]) }}">{{$comment->user->name}} ({{$comment->user->email}})</a> {{ $comment->user->status ? 'активен' : 'не активен' }}
                </td>
                <td>
                    {{$comment->created_at}}
                </td>
                <td>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['admin.comments.destroy', $comment->id],
                        'style' => 'display:inline',
                        'onsubmit' => 'return confirm(\'Подтверждаете удаление?\')'
                    ]) !!}
                    {{--<div class="btn-group btn-group-sm" role="group">--}}
                    {{--<a href="{{route('admin.articles.edit', $article->id)}}" class="btn btn-primary">Редактировать</a>--}}
                    {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}
                    {{--</div>--}}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('custom-js')
    <script src="{{ asset('js/data-table.js') }}"></script>
@endsection