@extends('layouts.admin')

@section('content')

    @include('admin.title', [
            'title' => 'Задачи',
            'items' => [
                ['title' => 'Задачи']
            ],
            'actions' => [
                [
                    'url' => '#',
                    'title' => 'Добавить задачу',
                    'atrs' => 'data-toggle="modal" data-target="#add_task_modal"'
                ]
            ]
        ])
    @include('admin.messages')
    @include('admin.tasks.modal')

    <table class="table table-hover">
        <thead>
            <th>Описание</th>
            <th>Исполнитель</th>
            <th>Оплата</th>
            <th>Добавлена</th>
            <th></th>
            <th></th>
        </thead>

        <tbody id="todo_list"></tbody>

        <tfoot>
            @include('admin.tasks.item-tpl')
        </tfoot>
    </table>

@endsection

@section('custom-js')
    <script src="{{ asset('adm/js/lib/jquery.tmpl.min.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
@endsection