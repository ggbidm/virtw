<!-- Modal -->
<div class="modal fade" id="add_task_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" id="task_add_form">
                {{ csrf_field() }}
                <input name="author_id" type="hidden" id="current_user_id" value="{{ \Auth::user()->id }}"/>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Новая задача</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="form-group">
                                <label for="todo_add_performer">Исполнитель</label>
                                <select name="performer_id" default="{{Auth::user()->id}}" required id="todo_add_performer" class="form-control" style="">
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form-group">
                                <label for="todo_add_priority">Приоритет</label>
                                <select name="priority" id="todo_add_priority" class="form-control" style="">
                                    <option data-class="bg-success" value="0"><label class="badge badge-success">Низкий</label></option>
                                    <option data-class="bg-warning" value="1"><label class="badge badge-warning">Средний</label></option>
                                    <option data-class="bg-danger" value="2"><label class="badge badge-danger">Высокий</label></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-xs-6">
                            <div class="form-group">
                                <label for="todo_add_type">Оплата</label>
                                <select name="type" id="todo_add_type" class="form-control" style="">
                                    <option value="0">Сдельная</option>
                                    <option value="1">Почасовая</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-xs-6">
                            <div class="form-group">
                                <label for="todo_add_price">Стоимость</label>
                                <input value="" id="todo_add_price" required name="price" data-bv-integer-message="Должно быть целым!" type="number" class="form-control"  placeholder="Стоимость" style="">
                            </div>
                        </div>
                    </div>
                    {{--<div class="row" id="todo_time_limit_box" style="display: none;">
                        <div class="col col-xs-2">
                            <div class="form-group">
                                <label>Авто-пауза через:</label>
                            </div>
                        </div>
                        <div class="col col-xs-5">
                            <div class="form-group">
                                <label for="todo_limit_h">Часов</label>
                                <select name="limit_h" id="todo_limit_h" class="form-control" style="">
                                    @for ($i = 0; $i <= 8; $i++)
                                        <option>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col col-xs-5">
                            <div class="form-group">
                                <label for="todo_limit_m">Минут</label>
                                <select name="limit_m" id="todo_limit_m" class="form-control" style="">
                                    @for ($i = 0; $i <= 59; $i++)
                                        <option>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>--}}
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="form-group">
                                <label for="todo_add_about">Описание</label>
                                <textarea name="about" rows="8" id="todo_add_about" required class="form-control text"  placeholder="Описание задачи"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</div>
