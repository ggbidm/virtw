<script id="todoItemTemplate" type="text/x-jquery-tmpl">
    <tr class="${statusClass}">
        <td {{if status == 2}}style="text-decoration: line-through;"{{/if}}>${text}</td>
        <td>${performer}</td>
        <td>{{if type==0}}${price}р.{{else}}${summ} р. (${price}р./ч.) {{/if}}</td>
        <td>${date}</td>
        <td class="todo-item-end" style="text-align: right;">
            {{if type == 1}}
                <div class="timer-box">
                    <div class="btn-group btn-todo-box btn-group-sm" data-toggle="buttons">
                        {{if isMy}}
                            <div style="width: 100px;" data-seconds="${seconds}" class="todo-time input-group-addon">${time}</div>
                            {{if status == 1}}
                                <button data-task-id="${id}" type="button" class="btn btn-primary btn-todo-play"><i class="glyphicon glyphicon-play"></i></button>
                            {{/if}}
                            {{if status == 0}}
                                <button data-task-id="${id}" type="button" class="btn btn-warning btn-todo-pause"><i class="glyphicon glyphicon-pause"></i></button>
                            {{/if}}
                        {{/if}}
                    </div>
                </div>
            {{/if}}
        </td>
        <td class="todo-item-end" style="text-align: right;">
            {{if isMy}}
                <div class="timer-box">
                    <div class="btn-group btn-todo-box btn-group-sm" data-toggle="buttons">
                        {{if status == 1 || status == 0}}
                            <button data-task-id="${id}" type="button" class="btn btn-todo-finish btn-success"><i class="glyphicon glyphicon-ok"></i></button>
                        {{/if}}
                        <button data-task-id="${id}" type="button" class="btn btn-todo-delete btn-danger"><i class="glyphicon glyphicon-remove"></i></button>
                    </div>
                </div>
            {{/if}}
        </td>
    </tr>
</script>