@extends('layouts.admin')

@section('title', 'Статьи')

@section('content')

    @include('admin.title', [
        'title' => 'Статьи',
        'items' => [
            ['title' => 'Список']
        ],
        'actions' => [
            [
                'url' => route('admin.articles.create'),
                'title' => 'Добавить'
            ]
        ]
    ])

    @include('admin.messages')

    <table id="data_table" class="table">
        <thead>
        <tr>
            <th style="width: 100px;"></th>
            <th>Заголовок</th>
            <th>Раздел</th>
            <th>Добавлена</th>
            <th style="width: 100px;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td style="height: 70px;">
                    @if($article->thumbnail_url)
                        <a target="_blank" href="{{ route('admin.articles.edit', $article->id) }}">
                            <img class="img-thumbnail" style="width: 100%; height: auto; border-radius: 0%;"  width="50" src="{{ $article->thumbnail_url }}"/>
                        </a>
                    @endif
                </td>
                <td>
                    <a href="{{route('admin.articles.edit', $article->id)}}">
                        {{$article->title}}
                    </a>
                </td>
                <td>
                    {{$article->category->title}}
                </td>
                <td>
                    {{$article->created_at}}
                </td>
                <td>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['admin.articles.destroy', $article->id],
                        'style' => 'display:inline',
                        'onsubmit' => 'return confirm(\'Подтверждаете удаление?\')'
                    ]) !!}
                    {{--<div class="btn-group btn-group-sm" role="group">--}}
                        {{--<a href="{{route('admin.articles.edit', $article->id)}}" class="btn btn-primary">Редактировать</a>--}}
                        {!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}
                    {{--</div>--}}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('custom-js')
    <script src="{{ asset('js/data-table.js') }}"></script>
@endsection
