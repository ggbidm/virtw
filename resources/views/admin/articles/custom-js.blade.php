@section('custom-js')
    @include('admin.tinymce-init')
    @include('admin.articles.form-validation')

    <script src="{{ asset('js/file-upload.js') }}"></script>
@endsection

            /*tinymce.init({
                selector: '#text-editor',
                height: 500,
                language: "ru",
                plugins: 'code  print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor insertdatetime advlist lists textcolor wordcount imagetools  contextmenu colorpicker textpattern jbimages',
                toolbar: 'preview code | link | undo redo | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | fullscreen | image jbimages',
                relative_urls: false,
                file_browser_callback: function(field_name, url, type, win) {
                    // trigger file upload form
                    if (type == 'image') $('#formUpload input').click();
                }
            });*/
