{!! Form::model($model, [
    'method' => ($model->id) ? 'PUT' : 'STORE',
    'route' => ($model->id) ? ['admin.articles.update', $model->id] : 'admin.articles.store',
    'style' => 'display:inline',
    'id' => 'model_form',
    'enctype' => 'multipart/form-data'
]) !!}

<div class="form-group">
    {!! Form::submit(($model->id) ? 'Сохранить' : 'Создать', ['class' => 'btn btn-success']) !!}
</div>

<ul class="nav nav-tabs tab-solid tab-solid-primary" role="tablist">
    <li class="nav-item">
        <a class="nav-link active show" id="tab-5-1" data-toggle="tab" href="#text" role="tab" aria-controls="home-5-1" aria-selected="true">Текст</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="tab-5-2" data-toggle="tab" href="#about" role="tab" aria-controls="profile-5-2" aria-selected="false">Описание</a>
    </li>
</ul>
<div class="tab-content tab-content-solid">
    <div class="tab-pane fade active show" id="text" role="tabpanel" aria-labelledby="tab-5-1">
        <div class="form-group">
            {!! Form::textarea('text', null, [
                'class' => 'form-control tinymce',
            ]) !!}
        </div>
    </div>
    <div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="tab-5-2">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('title', 'Название') !!}
                    {!! Form::text('title', null, [
                        'class' => 'form-control',
                        'id' => 'title'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('vk_title', 'Название для VK') !!}
                    {!! Form::text('vk_title', null, [
                        'class' => 'form-control',
                        'id' => 'vk_title'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-6">
                @if ($model->id)
                    <div class="form-group">
                        {!! Form::label('slug', 'Slug') !!}
                        {!! Form::text('slug', null, [
                            'class' => 'form-control',
                            'style' => 'disabled: disabled;',
                        ]) !!}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('source_title', 'Источник - Заголовок') !!}
                    {!! Form::text('source_title', null, [
                        'class' => 'form-control',
                        'id' => 'source_title'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('source_url', 'Источник - ссылка') !!}
                    {!! Form::text('source_url', null, [
                        'class' => 'form-control',
                        'id' => 'source_url'
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('title', 'Категория') !!}
                    {!! Form::select('category_id', $categories, null, [
                        'class' => 'form-control',
                    ]) !!}
                </div>
            </div>
            <div class="col-md-6">

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Картинка</label>
                    @if ($model->image_url)
                        <br>
                        <img style="width: 200px; margin: 10px 10px 10px 0;" src="{{$model->image_url}}"/>
                    @endif
                    <input type="file" name="image_url" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input name="image" type="text" class="form-control file-upload-info" disabled="" placeholder="">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Выбрать изображение</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Превьюшка</label>
                    @if ($model->thumbnail_url)
                        <br>
                        <img style="width: 200px; margin: 10px 10px 10px 0;" src="{{$model->thumbnail_url}}"/>
                    @endif
                    <input type="file" name="thumbnail_url" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input name="image" type="text" class="form-control file-upload-info" disabled="" placeholder="">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Выбрать изображение</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('anons', 'Анонс') !!}
            {!! Form::textarea('anons', null, [
                'class' => 'form-control tinymce',
            ]) !!}
        </div>


        <div class="form-group">
            {!! Form::label('meta_title', 'Мета-заголовок') !!}
            {!! Form::text('meta_title', null, [
                'class' => 'form-control',
            ]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('meta_keywords', 'Ключевые слова') !!}
            {!! Form::text('meta_keywords', null, [
                'class' => 'form-control',
            ]) !!}
        </div>

        <div class="form-group">
            {!! Form::label('meta_descriptions', 'Мета-описание') !!}
            {!! Form::text('meta_descriptions', null, [
                'class' => 'form-control',
            ]) !!}
        </div>
    </div>
</div>

{!! Form::close() !!}