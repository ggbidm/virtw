@extends('layouts.admin')

@section('title', 'Добавление статьи')

@section('content')

    @include('admin.messages')

    @include('admin.articles.form', [
        'model' => $model
    ])

@endsection

@section('custom-js')
    @include('admin.articles.custom-js')
@endsection
