<script>
    (function($) {
        $("#model_form").validate({
            rules: {
                title: "required",
            },
            messages: {
                title: "Обязательное поле",
            },
            errorPlacement: function (label, element) {
                label.addClass('mt-2 text-danger');
                label.insertAfter(element);
            },
            highlight: function (element, errorClass) {
                $(element).parent().addClass('has-danger')
                $(element).addClass('form-control-danger')
            }
        });
    })(jQuery);
</script>