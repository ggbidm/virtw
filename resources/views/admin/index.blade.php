@extends('layouts.admin')

@section('title', 'Главная')

@section('content')

    <div class="row">
        <div class="col-xxl-5 col-xl-6 col-md-9">
            <section class="widget widget-pie-chart">
                <div class="no-padding">
                    <div class="tbl tbl-grid text-center">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-30">
                                <div class="display-inline">
                                    @foreach($users as $user)
                                        <div class="chart-legend">
                                            <div class="circle red"></div>
                                            <div class="percent">{{ ceil(($user->getContribution() / $total) * 100 )}}%<sup>{{ $user->getContribution() }} р.</sup></div>
                                            <div class="caption">{{$user->name}}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tbl-cell tbl-cell-40">
                                <div class="ggl-pie-chart-container">
                                    <div id="donutchart" class="ggl-pie-chart"></div>
                                    <div class="ggl-pie-chart-title">
                                        <div class="caption">Всего</div>
                                        <div class="number">{{$total}}&nbspр.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!--.widget-pie-chart-->
        </div>
        <div class="col-md-6">
            <section class="widget widget-pie-chart">
                <div class="no-padding">
                    <div class="tbl tbl-grid text-center">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-30">
                                <div class="display-inline">
                                    <div id="yad"></div>
                                    <div>
                                        <a href="https://metrika.yandex.ru/dashboard?id=52314544">Подробная статистика</a> (Для доступа обратится к администратору)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection

@section('custom-js')
    <script src="{{ asset('https://www.gstatic.com/charts/loader.js') }}"></script>
@endsection
