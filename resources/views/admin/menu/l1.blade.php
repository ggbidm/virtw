<nav class="side-menu">
    <ul class="side-menu-list">
        @foreach($items as $i=>$item)
            @if(!$item->hasChildren())
                <li class="@if($item->isActive) opened @endif">
                    <a href="{!! $item->url() !!}">
                        @if(isset($item->data['icon']))
                            <i class="{{ $item->data['icon'] }}"></i>
                        @endif
                        <span class="lbl">{!! $item->title !!}</span>
                    </a>
                </li>
            @else
                <li class="{{--with-sub --}}@if($item->isActive) opened @endif">
                    @if($item->url()) <a href="{!! $item->url() !!}"> @endif
                        <span>
                            @if(isset($item->data['icon']))
                                <span class="{{ $item->data['icon'] }}"></span>
                            @endif
                            <span class="lbl">{!! $item->title !!}</span>
                        </span>
                    @if($item->url()) </a> @endif

                    {{--@include('admin.menu.l2', ['items' => $item->children()])--}}
                </li>
            @endif
        @endforeach
    </ul>
</nav>

