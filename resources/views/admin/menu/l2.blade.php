<ul class="">
    @foreach($items as $item)
        <li>
            <a class="@if($item->isActive) active @endif" href="{!! $item->url() !!}">
                <span class="lbl">{!! $item->title !!}</span>
            </a>
        </li>
    @endforeach
</ul>
