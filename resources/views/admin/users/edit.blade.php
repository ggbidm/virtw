@extends('layouts.admin')

@section('title', 'Редактирование пользователя')

@section('content')

    @include('admin.messages')

    @include('admin.users.form', [
        'model' => $model
    ])

@endsection