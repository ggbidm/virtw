@extends('layouts.admin')

@section('title', 'Статьи')

@section('content')

    @include('admin.title', [
        'title' => 'Пользователи',
        'items' => [
            ['title' => 'Список']
        ],
        'actions' => [
            /*[
                'url' => route('admin.articles.create'),
                'title' => 'Добавить'
            ]*/
        ]
    ])

    @include('admin.messages')

    <table id="data_table" class="table">
        <thead>
        <tr>
            <th>Имя</th>
            <th>EMail</th>
            <th>Статус</th>
            <th>Зарегистрирован</th>
            <th style="width: 100px;"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    {{ $user->name }}
                </td>
                <td>
                    {{ $user->email }}
                </td>
                <td>
                    {{ $user->status ? 'активен' : 'не активен' }}
                </td>
                <td>
                    {{$user->created_at}}
                </td>
                <td>
                    {{--{!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['admin.users.destroy', $user->id],
                        'style' => 'display:inline',
                        'onsubmit' => 'return confirm(\'Подтверждаете удаление?\')'
                    ]) !!}--}}
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-primary">Редактировать</a>
                        {{--{!! Form::submit('Удалить', ['class' => 'btn btn-danger btn-sm']) !!}--}}
                    </div>
                    {{--{!! Form::close() !!}--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection

@section('custom-js')
    <script src="{{ asset('js/data-table.js') }}"></script>
@endsection
