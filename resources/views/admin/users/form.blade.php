{!! Form::model($model, [
    'method' => ($model->id) ? 'PUT' : 'STORE',
    'route' => ($model->id) ? ['admin.users.update', $model->id] : 'admin.users.store',
    'style' => 'display:inline',
    'id' => 'model_form',
    'enctype' => 'multipart/form-data'
]) !!}

    <div class="form-group">
        {!! Form::submit(($model->id) ? 'Сохранить' : 'Создать', ['class' => 'btn btn-success']) !!}
        {{ csrf_field() }}
    </div>
    <div class="form-check">
        <label class="form-check-label">
            <input type="checkbox" name="status" {{ $model->status ? 'checked' : '' }} class="form-check-input">
            Статус
        </label>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('name', 'Имя') !!}
                {!! Form::text('name', null, [
                    'class' => 'form-control',
                    'id' => 'name',
                    'style' => 'pointer-events: none;',
                ]) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'EMail') !!}
                {!! Form::text('email', null, [
                    'class' => 'form-control',
                    'id' => 'email',
                    'style' => 'pointer-events: none;',
                ]) !!}
            </div>
        </div>
    </div>

{!! Form::close() !!}