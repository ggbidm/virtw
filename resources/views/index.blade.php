@extends('layouts.main')

@section('meta_tags')

    @php
        $title = 'Virtual worlds - Все публикации';
        if ($page = Request::get('page')) {
            $title .= ', стр. '.$page;
        }

    @endphp

    <title>{{$title}}</title>
    <meta name="description" content="{{$title}}">
    <meta name="keywords" content="{{$title}}">

    <meta property="og:description" content="Virtual worlds - посвящен всему что связано с технологиями погружения в виртульные реальности" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="ru-ru" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Virtual worlds')}}" />
    <meta property="og:image" content="{{url()->current()}}/images/og-logo.png" />

    {{--<meta name='description' itemprop='description' content='Virtual worlds - ' />
    <meta name='keywords' content='{{$tags}}' />
    <meta property='article:published_time' content='{{$obj->created_at}}' />
    <meta property='article:section' content='event' />

    <meta property="og:description" content="{{$obj->description}}" />
    <meta property="og:title" content="{{$obj->title}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="en-us" />
    <meta property="og:locale:alternate" content="en-us" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Site Name')}}" />

    @foreach($obj->images as $image)
        <meta property="og:image" content="{{$image->url}}" />
        @endforeach
        <meta property="og:image:url" content="{{$obj->image}}" />
        <meta property="og:image:size" content="300" />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="{{$obj->title}}" />
        <meta name="twitter:site" content="@BrnBhaskar" />
    --}}
@endsection

@section('title', 'Главная')


@section('content')

    @include('partials.list')

    {{--@foreach($categories as $category)
        @if (count($articles = $category->lastArticles()) > 0)
            <div class="row grid-margin">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-sm-10">
                                    <h2>{{ $category->title }}</h2>
                                </div>
                                <div class="col col-sm-2 text-right">
                                    <a href="{{ route('category', ['category' => $category->slug]) }}" class="btn btn-outline-primary btn-xs float-right" style="">
                                        Все статьи
                                    </a>
                                </div>
                            </div>
                            <div class="owl-carousel owl-theme loop">
                                @foreach($articles as $article)
                                    <div class="item">
                                        @if($article->thumbnail_url)
                                            <a href="{{ route('article', ['category' => $category->slug, 'article' => $article->slug]) }}">
                                                <img src="{{ $article->thumbnail_url }}" alt="banner image"/>
                                            </a>
                                        @endif

                                        {!! $article->showCreatedDate() !!}
                                        <div class="clearfix"></div>

                                        <a href="{{ route('article', ['category' => $category->slug, 'article' => $article->slug]) }}">
                                            <h6>{{ $article->title }}</h6>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach--}}

@endsection

@section('custom-js')
    <script>
        (function($) {
            'use strict';

            $.fn.andSelf = function() {
                return this.addBack.apply(this, arguments);
            }

            console.log($('.owl-carousel').length);
            $('.owl-carousel').owlCarousel({
                loop: false,
                margin: 10,
                autoplay: false,
                autoplayTimeout: 4500,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
        })(jQuery);
    </script>

@endsection

@section('custom-css')
    <style>
        .badge-comment {
            font-size: 12pt !Important;
            color: #070d4c5e !Important
        }
    </style>
@endsection