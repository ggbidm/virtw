<ul class="nav page-navigation">
    @foreach($items as $item)
        <li class="nav-item
            @if($item->isActive)
                active
            @endif
        "><a class="nav-link" href="{!! $item->url() !!}">{!! $item->title !!}</a></li>
    @endforeach
</ul>
