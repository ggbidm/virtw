@foreach($articles as $article)
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row custom-a">
                        <div class="col col-sm-11">
                            <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}">
                                <h4 style="font-size: 1.6rem; margin-bottom: 0; color: #656567;" class="card-title">{{ $article->title }}</h4>
                            </a>
                        </div>
                        <div class="col col-sm-1 pull-right">
                            <a title="{{ $article->title }} - Комментарии ({{ $article->comments->count() }})" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}#comments"> <div style="color: #cec9c9;" class="float-right badge badge-comment badge-pill badge-outline-default"><i class="icon-bubble"></i> {{ $article->comments->count() }}</div></a>
                        </div>
                    </div>
                    <div class="row custom-a" style="margin-bottom: 25px; margin-top: 5px;">
                        <div class="col col-sm-12">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb" style="padding: 0; margin: 0; ">
                                    <li class="breadcrumb-item"><a title="На главную" href="/">Главная</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        <a title="{{ $article->category->title }}" href="{{ route('category', ['slug' => $article->category->slug]) }}">{{ $article->category->title }}</a>
                                    </li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                    @if($article->image_url)
                        <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}">
                            <img alt="{{ $article->title }}" style="width: 100%;" src="{{ $article->image_url }}"/>
                        </a>
                    @endif

                    <div style="margin-top: 5px;/*text-align: justify;*/">
                        {!! $article->anons !!}
                    </div>

                    {!! $article->showCreatedDate() !!}

                    <a title="{{ $article->title }}" href="{{ route('article', ['categories' => $article->category->slug, 'article' => $article->slug]) }}" class="btn btn-outline-primary float-right" style="margin-top: 10px;">
                        Читать далее
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach

{{ $articles->links('paginate') }}

@include('partials.banner-yandex')