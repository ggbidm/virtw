<ul class="nav justify-content-center page-navigation custom-a">
    @foreach($items as $item)
        <li class="nav-item
            @if($item->isActive)
                active
            @endif
        ">
            <a class="nav-link" href="{!! $item->url() !!}">
                {{--<i class="link-icon icon-{!! $item->icon !!}"></i>--}}<span class="menu-title">{!! $item->title !!}@if($item->hasChildren())<i class="menu-arrow"></i>@endif</span>
            </a>
            @if($item->hasChildren())
                <div class="submenu">
                    <ul class="submenu-item">
                        @include('partials.menu-l2', ['items' => $item->children()])
                    </ul>
                </div>
            @endif
        </li>
    @endforeach
</ul>
