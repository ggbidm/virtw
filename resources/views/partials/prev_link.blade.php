<table width="100%">
    <tr>
        <td width="40" class="align-middle">
            <a class="no-decor" href="{{ route('article', ['category' => $article->category->slug, 'article' => $article->slug]) }}">
                <i class="icon-arrow-left icon-lg text-warning"></i>
            </a>
        </td>
        <td class="align-middle">
            <a class="no-decor" href="{{ route('article', ['category' => $article->category->slug, 'article' => $article->slug]) }}">
                <h4>{{ $article->title }}</h4>
                <div>{!! $article->showCreatedDate() !!}</div>
                <br>
                <img class="{{--img-thumbnail--}} img-fluid" height="160" width="256" src="{{$article->thumbnail_url}}"/>
            </a>
        </td>
    </tr>
</table>