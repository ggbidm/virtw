@extends('layouts.main')

@section('meta_tags')

    <title>Virtual worlds - О нас</title>
    <meta name="description" content="Virtual worlds - О нас">

    <meta property="og:description" content="Virtual worlds - О нас" />
    <meta property="og:title" content="Virtual worlds - О нас" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:type" content="article" />
    <meta property="og:locale" content="ru-ru" />
    <meta property="og:site_name" content="{{env('SITE_URL', 'Virtual worlds')}}" />
    <meta property="og:image" content="https://virtw.ru/images/og-logo.jpg" />
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-sm-11">
                            <h1>О нас</h1>
                            <p><strong>Здравствуйте! Приветствуем вас!</strong></p>
                            <p>В данный момент вы находитесь на сайте <a href="https://virtw.ru">Virtual Words</a>, посвященному технологиям создания виртуальных миров, обзорам этих миров и средствам погружения в них.</p>
                            <p>Мы - группа энтузиастов, которым интересна эта тема.</p>
                            <p>На нашем сайте вы сможете найти <a href="/obzory">обзоры</a> девайсов для погружения в VR/AR/MR среды а так же обзоры этих самых сред. Возможно сможете найти что-то полезное в разделе <a href="/gaydy">гайды</a>. Еще мы публикуем <a href="/novosti">новости</a>, так или иначе касающиеся виртуальных миров.</p>
                            <p>&nbsp;</p>
                            <p>Приятного времяпрепровождения.</p>
                            <p>С уважением, администрация сайта.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection