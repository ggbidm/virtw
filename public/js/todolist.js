todo = {
    box: '#todo_list',
    tpl: "#todo_item_tpl",
    form: '#task_add_form',
    timer: false,

    init: function () {
        var _this = this;
        $(_this.box).html('');
        $.ajax({
            url: '/admin/tasks/list',
            method: "get",
            dataType: 'JSON',
            success: function(data){
                $(data.tasks).each(function(i, e){
                    _this.drawTask(e);
                });
            }
        });
    },

    addTask: function() {
        var _this = this;
        $.ajax({
            url: '/admin/tasks/add',
            method: "post",
            data: $(this.form).serialize(),
            success: function(){
                _this.init();
            }
        });
    },

    startTask: function(id) {
        var _this = this;
        $.ajax({
            url: '/admin/tasks/start/' + id,
            method: "get",
            success: function(){
                _this.init();
            }
        });
    },

    stopTask: function(id) {
        var _this = this;
        $.ajax({
            url: '/admin/tasks/stop/' + id,
            method: "get",
            success: function(){
                _this.init();
            }
        });
    },

    finishTask: function(id) {
        var _this = this;
        $.ajax({
            url: '/admin/tasks/finish/' + id,
            method: "get",
            success: function(){
                _this.init();
            }
        });
    },

    deleteTask: function(id) {
        var _this = this;
        if (confirm('Подтверждаете удаление?')) {
            $.ajax({
                url: '/admin/tasks/delete/' + id,
                method: "get",
                success: function(){
                    _this.init();
                }
            });
        }

    },

    getStatusClass: function (status) {
        switch (status) {
            case 0: return 'table-active'; // в работе
            case 1: return ''; // не в работе
            case 2: return ''; // закрытые
        }
    },

    drawTask: function (data) {
        var task = $('#todoItemTemplate').tmpl({
            id: data.id,
            text: data.about,
            performer: data.performer.name,
            price: data.price,
            summ: (data.price * (data.seconds / 3600)).toFixed(0),
            date: data.date,
            time: getTimeBySeconds(data.seconds),
            seconds: data.seconds,
            type: data.type.id,
            status: data.status.id,
            isMy: ($('#current_user_id').val() == data.performer.id),
            statusClass: this.getStatusClass(data.status.id)
        });

        if (data.status.id == 0) {
            this.timer = setInterval(function() {
                var s = task.find('.todo-time').attr('data-seconds');
                s++;
                task.find('.todo-time').html(getTimeBySeconds(s));
                task.find('.todo-time').attr('data-seconds', s);
            }, 1000);
        }

        task.find('.btn-todo-pause').on('click', function() {
            todo.stopTask($(this).attr('data-task-id'));
        });

        task.find('.btn-todo-play').on('click', function() {
            todo.startTask($(this).attr('data-task-id'));
        });

        task.find('.btn-todo-finish').on('click', function() {
            todo.finishTask($(this).attr('data-task-id'));
        });

        task.find('.btn-todo-delete').on('click', function() {
            todo.deleteTask($(this).attr('data-task-id'));
        });

        task.appendTo(this.box);
        return;
        var task = $(this.tpl).clone();

        var timerBox = task.find('.timer-box');
        var timerInfo = task.find('.todo-timer');
        var btnPlay = task.find('.btn-todo-play');
        var btnPause = task.find('.btn-todo-pause');


        task.find('.todo-item-1').html(data.id);
        task.find('.todo-item-2').html(data.about);
        task.find('.todo-item-3').html(data.performer.name);
        task.find('.todo-item-5').html(data.date);
        task.removeClass('table-active');

        // Чужие задачи, блокиреум управление задачей
        if (currentUserId != data.performer.id) {
            task.find('.todo-item-end').hide();

        // Свои задачи
        } else {
            // Почасовая
            if (data.type.id == 1) {
                task.find('.todo-item-time').html(getTimeBySeconds(data.seconds));
                task.find('.todo-item-4').html(data.price + ' р/ч (почасовая)');

                switch (data.status.id) {
                    // в работе
                    case 0:
                        console.log(data.status.id);
                        task.addClass('table-active');
                        btnPause.hide();
                        break;

                    // не в работе
                    case 1:
                        console.log(data.status.id);
                        task.addClass('table-primary');
                        btnPlay.hide();
                        break;

                    // закрыта
                    default:
                        task.addClass('table-success');
                        btnPlay.hide();
                        btnPause.hide();
                }

            // Сделная
            } else {
                btnPlay.hide();
                btnPause.hide();
                task.find('.todo-item-4').html(data.price + 'р (сдельная)');
            }
        }

        task.show();
        $(this.box).append(task);
    },
};

function getTimeBySeconds(sec) {
    var h = sec/3600 ^ 0 ;
    var m = (sec-h*3600)/60 ^ 0 ;
    var s = sec-h*3600-m*60 ;
    return (h<10?"0"+h:h)+":"+(m<10?"0"+m:m)+":"+(s<10?"0"+s:s);
}

function formatDate(date) {
    var d = new Date(date.date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        h = d.getHours(),
        m = d.getMinutes();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return day + '.' + month + '.' + year + ' ' + h + ':' + m;
}

(function ($) {
    'use strict';

    $(function () {
        todo.init();
        /*$('#todo_add_type').on('change', function () {
            if ($(this).val() == "0") {
                $('#todo_time_limit_box').hide();
            } else {
                $('#todo_time_limit_box').show();
            }
        });*/

        $('#task_add_form').on('submit', function () {
            todo.addTask();

            $('#todo_add_performer').val($('#todo_add_performer').attr('default'));
            $('#todo_add_type').val(0);
            $('#todo_time_limit_box').hide();
            $('#todo_add_priority').val(0);
            //$('#todo_add_price').val('');
            //$('#todo_add_about').val('');

            $('#add_task_modal').modal('hide');
            return false;
        });
    });
})(jQuery);