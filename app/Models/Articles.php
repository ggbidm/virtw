<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed category_id
 * @property mixed created_at
 * @property mixed vk_is_public
 * @property mixed slug
 * @property mixed category
 */
class Articles extends Model
{
    protected $perPage = 10;
    protected $vkKey = '01e71ddb60531a00ca40ea8bd217221162e122679c1884a8b3d7b13e9395943582e2fa8003f2b5b8a41a4';
    protected $vkAppId = '7311616';
    protected $vkGroupId = '184351923';
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['source_title', 'source_url', 'category_id', 'title', 'vk_title', 'anons', 'text', 'meta_title', 'meta_keywords', 'meta_descriptions', 'slug'];

    public function vkGetToken() {
        return $this->vk();
    }
    public function vkAddTopic()
    {
        if (!$this->vk_is_public) {
            return $this->vk('wall.post', [
                'group_id' => $this->vkGroupId,
                'scope' => 'wall,photos',
                'title' => $this->title,
                'text' => 'https://virtw.ru/' . route('article', ['category' => $this->category->slug, 'article' => $this->slug]),
                'from_group' => '1',
                //'attachments' => 'photo100172_166443618,photo66748_265827614'
            ]);
        }
    }

    public function vk($method, $params)
    {
        $params['access_token'] = $this->vkKey;
        $params['access_token'] = 'a0d0b75fa0d0b75fa0d0b75fc3a0bf265faa0d0a0d0b75ffeedde189435ae5ddc98984c';
        //$params['v'] = '5.62';
        $url = 'https://api.vk.com/method/'.$method;

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return $result;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Categories');
    }

    public function getPrevLink()
    {
        $article = Articles::where([
            'category_id' => $this->category_id
        ])->where('created_at', '<', $this->created_at)->orderBy('created_at', 'desc')->first();

        if ($article) {
            echo view('partials.prev_link', [
                'article' => $article
            ]);
        }
    }

    public function getNextLink()
    {
        $article = Articles::where([
            'category_id' => $this->category_id
        ])->where('created_at', '>', $this->created_at)->orderBy('created_at', 'asc')->first();

        if ($article) {
            echo view('partials.next_link', [
                'article' => $article
            ]);
        }
    }

    public function showCreatedDate()
    {
        return view('partials.created_date', [
            'date' => date('d.m.Y', strtotime($this->created_at))
        ]);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    /**
     * Get the relationships for the entity.
     *
     * @return array
     */
    public function getQueueableRelations()
    {
        // TODO: Implement getQueueableRelations() method.
    }
}
