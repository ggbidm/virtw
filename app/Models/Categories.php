<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Categories extends Model
{
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $fillable = ['title', 'meta_title', 'meta_keywords', 'meta_descriptions', 'slug'];

    public function articles() {
        return $this->hasMany('App\Models\Articles', 'category_id');
    }

    public function lastArticles($count=100) {
        return $this->articles()->orderBy('created_at', 'DESC')->limit($count)->get();
    }

    /**
     * Возвращает дату последнего обновления статей
     * @return Carbon
     */
    public static function getLastDateUpdate() {
        $date = null;

        foreach (Categories::all() as $category) {
            foreach ($category->articles->all() as $article) {
                if (is_null($date)) $date = $article->created_at;
                else {
                    if ($date->lt($article->created_at)) $date = $article->created_at;
                }
            }
        }

        return $date;
    }

    /**
     * Возвращает дату последнего обновления статей текущей категории
     * @return Carbon
     */
    public function lastDateUpdate() {
        $date = null;
        foreach ($this->articles->all() as $article) {
            if (is_null($date)) $date = $article->created_at;
            else {
                if ($date->lt($article->created_at)) $date = $article->created_at;
            }
        }
        return $date;
    }
}
