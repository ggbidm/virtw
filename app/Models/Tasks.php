<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User author
 * @property User performer
 * @property mixed status
 * @property mixed type
 * @property mixed priority
 * @property mixed price
 * @property mixed seconds
 * @property mixed limit_min
 * @property mixed about
 * @property mixed created_at
 * @property mixed started_at
 * @property mixed performer_id
 */
class Tasks extends Model
{
    public $statuses = [
        0 => 'В работе',
        1 => 'Не в работе',
        2 => 'Завершена',
    ];

    public $types = [
        0 => 'Сделная оплата',
        1 => 'Почасовая оплата',
    ];

    public $priorities = [
        0 => 'низкий',
        1 => 'средний',
        2 => 'высокий',
    ];

    protected $fillable = ['author_id', 'performer_id', 'about', 'limit_min', 'seconds', 'type', 'price', 'status', 'priority', 'started_at'];

    public function author() {
        return $this->belongsTo('App\User', 'author_id');
    }

    public function performer() {
        return $this->belongsTo('App\User', 'performer_id');
    }

    public function stop() {
        if ($this->status == 0 && $this->performer_id == \Auth::user()->id && $this->type == 1) {
            $secondsStart = strtotime($this->started_at);
            $deltaTime = time() - $secondsStart;
            if ($secondsStart) {
                $this->seconds = $this->seconds + $deltaTime;
            }
            $this->status = 1;
            $this->save();
        }
    }

    public function getDeltaTime() {
        if ($this->status == 0 && $this->performer_id == \Auth::user()->id && $this->type == 1) {
            $secondsStart = strtotime($this->started_at);
            if (!$secondsStart) {
                $this->started_at = date('Y-m-d H:i:s');
                $this->save();
                $secondsStart = time();
            }

            $deltaTime = time() - $secondsStart;
            return $this->seconds + $deltaTime;
        }
        else return $this->seconds;
    }

    public function getData() {
        return array(
            'status' => array(
                'id' => $this->status,
                'name' => $this->statuses[intval($this->status)],
            ),
            'type' => array(
                'id' => $this->type,
                'name' => $this->types[intval($this->type)],
            ),
            'priority' => array(
                'id' => $this->priority,
                'name' => $this->priorities[intval($this->priority)],
            ),
            'author' => array(
                'id' => $this->author->id,
                'name' => $this->author->name,
            ),
            'performer' => array(
                'id' => $this->performer->id,
                'name' => $this->performer->name,
            ),
            'price' => $this->price,
            'seconds' => $this->getDeltaTime(),
            'about' => $this->about,
            'date' => date('H:i d.m.Y', strtotime($this->created_at)),
            'id' => $this->id,
            'limit' => array(
                'h' => floor($this->limit_min / 60),
                'm' => $this->limit_min % 60
            ),
        );
    }
}


