<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed id
 * @property mixed name
 * @property mixed tasks
 */
class User extends Authenticatable
{
    use Notifiable;

    public function tasks() {
        return $this->hasMany('App\Models\Tasks', 'performer_id');
    }

    public function getContribution() {
        $summ = 0;
        foreach ($this->tasks as $task) {
            if ($task->status == 2)
            if ($task->type == 0) $summ += $task->price;
            else {
                $summ += ceil($task->price * ($task->seconds / 3600));
            }
        }

        return $summ;
    }

    public function isAdmin() {
        return in_array($this->id, [1, 2, 3]);
    }

    public function isActive() {
        return $this->status;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
