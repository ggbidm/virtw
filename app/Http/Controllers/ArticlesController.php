<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Categories;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index()
    {
        return view('index', [
            'articles' => Articles::orderBy('created_at', 'DESC')->paginate()
        ]);
    }

    public function category(Request $request)
    {
        $category = Categories::whereSlug($request->category)->firstOrFail();
        if (!$category) abort(404);
        $articles = $category->articles()->orderBy('created_at', 'DESC')->paginate();
        return view('category', [
            'articles' => $articles,
            'category' => $category
        ]);
    }

    public function article(Request $request)
    {
        $article = Articles::where([
            'slug' => $request->article
        ])->get()->first();

        if (!$article) abort(404);

        pre($article->vkAddTopic());

        $article->views++;
        $article->save();

        return view('article', [
            'article' => $article
        ]);
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $message = '';
        if (strlen($query)<3) $message = 'Поисковй запрос слишком короткий (не менее 3-х символов)';
        if (strlen($query)>255) $message = 'Поисковй запрос слишком длинный (не более 255-х символов)';

        $result = collect([]);
        if ($message == '') {
            $result = Articles::where('title', 'LIKE', '%'.$query.'%')->orWhere('text', 'LIKE', '%'.$query.'%')->orWhere('anons', 'LIKE', '%'.$query.'%')->get();
        }

        return view('search', [
            'articles' => $result,
            'query' => $query,
            'message' => $message
        ]);
    }

    public function aboutUs()
    {
        return view('about-us');
    }

    public function sitemap() {
        $contents = view('sitemap', [
            'indexUpdateDate' => Categories::getLastDateUpdate(),
            'articlesPagination' => Articles::paginate(),
            'categories' => Categories::all(),
        ]);
        return response($contents)->header('Content-Type', 'Content-Type : text/xml');
    }
}
