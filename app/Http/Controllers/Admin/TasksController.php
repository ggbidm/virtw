<?php

namespace App\Http\Controllers\Admin;

use App\Models\Articles;
use App\Models\Categories;
use App\Models\Tasks;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tasks.index', [
            'users' => User::whereIn('id', [1, 2, 3])->get(),
            'tasks' => []
        ]);
    }

    public function taskList()
    {
        $tasks = Tasks::orderBy('status','asc')->orderBy('updated_at', 'desc')->get();

        $list = [];
        foreach ($tasks as $task) {
            $list[] = $task->getData();
        }

        return json_encode([
            'tasks' => $list
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $task = Tasks::create($request->all());
        $task->status = 1;
        //$task->limit_min = intval($data['limit_h']) * 60 + intval($data['limit_m']);
        $task->save();
    }

    public function start($id)
    {
        // Останавливаем запущенные
        foreach (Tasks::where([
            'performer_id' => \Auth::user()->id,
            'status' => 0
        ])->get() as $task) {
            $task->stop();
        }

        Tasks::find($id)->update([
            'status' => 0,
            'started_at' => date('Y-m-d H:i:s')
        ]);

        return;
    }

    public function stop($id)
    {
        Tasks::find($id)->stop();

        return;
    }

    public function finish($id)
    {
        $task = Tasks::find($id);
        $task->stop();
        $task->status = 2;
        $task->save();

        return;
    }

    public function delete($id)
    {
        Tasks::find($id)->delete();
        return;
    }
}
