<?php

namespace App\Http\Controllers\Admin;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function index() {
        return view('admin.comments.index', [
            'comments' => Comment::orderBy('created_at', 'DESC')->get()
        ]);
    }

    public function destroy($id)
    {
        Comment::find($id)->delete();

        return redirect()->route('admin.comments.index')
            ->with('success','Комментарий удален');
    }
}
