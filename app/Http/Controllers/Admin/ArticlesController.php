<?php

namespace App\Http\Controllers\Admin;

use App\Models\Articles;
use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.articles.index', [
            'articles' => Articles::OrderBy('created_at', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create', [
            'model' => new Articles(),
            'categories' => Categories::all()->pluck('title', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
        ]);

        $article = Articles::create($request->all());
        $article->save();

        if ($request->file('image_url')) {
            $image = $request->file('image_url');

            $image->move(public_path('img/main'), $article->slug.'.gif');
            $article->image_url = '/img/main/'.$article->slug.'.gif';
            $article->save();
        }

        if ($request->file('thumbnail_url')) {
            $image = $request->file('thumbnail_url');

            $image->move(public_path('img/thumbnail'), $article->slug.'.gif');
            $article->thumbnail_url = '/img/thumbnail/'.$article->slug.'.gif';
            $article->save();
        }

        return redirect(route('admin.articles.index'))->with('success', 'Статья добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.articles.edit', [
            'model' => Articles::find($id),
            'categories' => Categories::all()->pluck('title', 'id')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.articles.edit', [
            'model' => Articles::find($id),
            'categories' => Categories::all()->pluck('title', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'title' => 'required',
        ]);
        $article = Articles::find($id);
        $article->update($request->all());

        if ($request->file('image_url')) {
            $image = $request->file('image_url');

            $image->move(public_path('img/main'), $article->slug.'.gif');
            $article->image_url = '/img/main/'.$article->slug.'.gif';
            $article->save();
        }

        if ($request->file('thumbnail_url')) {
            $image = $request->file('thumbnail_url');

            $image->move(public_path('img/thumbnail'), $article->slug.'.gif');
            $article->thumbnail_url = '/img/thumbnail/'.$article->slug.'.gif';
            $article->save();
        }

        return redirect(route('admin.articles.index'))->with('success', 'Статья сохранена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Articles::find($id)->delete();

        return redirect()->route('admin.articles.index')
            ->with('success','Статья удалена');
    }
}
