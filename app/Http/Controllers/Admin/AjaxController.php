<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;

class AjaxController extends Controller
{
    public function loadImage(Request $request) {
        $image = $request->file('image');
        $fileName = $image->getClientOriginalName();
        $image->move(public_path('img'), $fileName);

        return mce_back($fileName);
    }
}
