<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tasks;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use phpQuery;

function parse() {
    $result = \GuzzleHttp\json_decode(file_get_contents('gsj.txt'));
    echo '<pre>';

    $sites = [];
    foreach($result as $e) {
        $url = parse_url('http://vr.loc'.$e->url);

        if (isset($url['query']) && $url['query']) {
            parse_str($url['query'], $query);
            if ($query['q'] != '' && $query['q'] != 'vr') {
                $url = parse_url($query['q']);
                //print_r($url);
                if (!isset($sites[$url['host']])) $sites[$url['host']] = 1;
                else $sites[$url['host']]++;
                echo sprintf('<div><a target="_blank" href="%s">%s</a><br>%s</div><br>',
                    $query['q'],
                    $e->title,
                    $e->about
                );
            }
        }
    }
    //pre($sites);
    die;

    //$html = get_out_xml("http://www.google.com./search?lr=lang_en&num=1000&q=vr");
    //file_put_contents('gs.txt', $html);
    //$html = file_get_contents('gs.txt');

    //$p = phpQuery::newDocument($html);
    //$result = [];

    $blocks = $p->find('.g');
    foreach ($blocks as $rc) {
        $result[] = [
            'title' => trim(strip_tags(pq($rc)->find('.r a')->html())),
            'about' => trim(strip_tags(pq($rc)->find('.st')->html())),
            'url' => pq($rc)->find('.r a')->attr('href'),
        ];
    }

    //file_put_contents('gsj.txt', \GuzzleHttp\json_encode($result));
    pre($result);
}

function get_out_xml($url,$data=null,$options=null)
{

    $process = curl_init($url);
    //$proxy = '75.183.130.158';
    //$proxyauth = 'user:password';
    curl_setopt($process, CURLOPT_HEADER,0);
    if(!is_null($data))
    {
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $data);
    }
    if(!is_null($options))curl_setopt_array($process,$options);
    curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($process, CURLOPT_COOKIEFILE, 'cookies.txt');
    curl_setopt($process, CURLOPT_COOKIEJAR, 'cookies.txt');
    curl_setopt($process, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5');
    curl_setopt ($process , CURLOPT_REFERER , 'http://google.com/');
    curl_setopt($process, CURLOPT_CONNECTTIMEOUT,20);
    @curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
    //curl_setopt($process, CURLOPT_PROXY, $proxy);
    //curl_setopt($process, CURLOPT_PROXYUSERPWD, $proxyauth);
    $return = curl_exec($process);
    curl_close($process);
//parser_sleep();
    return $return;
}

class IndexController extends Controller
{
    public function index() {
        $total = 0;
        foreach (Tasks::where([
            'status' => 2
        ])->get() as $task) {
            if ($task->type == 0) $total += $task->price;
            else {
                $total += ceil($task->price * ($task->seconds / 3600));
            }
        }

        if (!$total) $total = 1;

        return view('admin.index', [
            'users' => User::whereIn('id', [1,2,3])->get(),
            'total' => $total
        ]);
    }

    public function yadVisits() {
        $data = \YandexMetrika::getVisitsViewsUsers(7)->adapt()->data['data'];
        $json = [['', 'Посещения', 'Страницы', 'Пользователи']];

        foreach ($data as $item) {
            $json[] = [
                $item['dimensions'][0]['name'],
                $item['metrics'][0],
                $item['metrics'][1],
                $item['metrics'][2],
            ];
        }

        return json_encode($json);
    }

    public function keys() {
        $lines = file('text.txt');
        $result = [];
        foreach($lines as $line) {
            $l = explode(chr(9), $line);
            if (!isset($result[$l[0]])) {
                if (!isset($l[1])) vd($l);
                $result[$l[0]] = intval(str_replace(' ', '', $l[1]));
            }
        }
        arsort($result);
        foreach ($result as $k=>$v) echo $v.'<br>';
        die;
    }
}
