<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request->get('body');
        $comment->user()->associate($request->user());
        $article = Articles::find($request->get('model_id'));
        $article->comments()->save($comment);

        return redirect(redirect()->getUrlGenerator()->previous().'#comments');
    }
}
