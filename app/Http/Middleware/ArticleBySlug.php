<?php

namespace App\Http\Middleware;

use App\Models\Articles;
use Closure;

class ArticleBySlug
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($article = Articles::where([
            'slug' => $request->slug
        ])->get()->first()) {
            return $next($request);
        }

        echo view('404');
    }
}
