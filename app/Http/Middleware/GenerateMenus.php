<?php

namespace App\Http\Middleware;

use App\Models\Categories;
use Closure;
use Lavary\Menu\Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeName = \Request::route()->getName();
        if (strpos($routeName, 'admin.') === 0) {
            \Menu::make('menu', function ($menu) {
                $menu->add('Главная', [
                    'route' => 'admin.index',
                    'class' => ''
                ])->data('icon', 'font-icon font-icon-home');

                $menu->add('Задачи', [
                    'route' => 'admin.tasks',
                    'class' => 'nav-item'
                ])->data('icon', 'font-icon font-icon-fire');

                $cats = $menu->add('Категории', route('admin.categories.index'))->data('icon', 'font-icon font-icon-folder');
                $cats->add('Список', route('admin.categories.index'));
                $cats->add('Добавить', route('admin.categories.create'));

                $articles = $menu->add('Статьи', route('admin.articles.index'))->data('icon', 'font-icon font-icon-page');
                $articles->add('Список', route('admin.articles.index'));
                $articles->add('Добавить', route('admin.articles.create'));

                $menu->add('Комментарии', route('admin.comments.index'))->data('icon', 'font-icon font-icon-comments');
                $menu->add('Пользователи', route('admin.users.index'))->data('icon', 'font-icon font-icon-users');

                $menu->add('Перейти на сайт', [
                    'route' => 'index',
                    'class' => 'nav-item'
                ])->data('icon', 'font-icon font-icon-earth-bordered');
            });

        } else {
            \Menu::make('menu', function ($menu) {
                $menu->add('Главная');

                foreach (Categories::all() as $category) {
                    if ($category->articles->count() > 0) {
                        $menu->add($category->title, route('category', ['category' => $category->slug]));
                    }
                }

                $menu->add('О нас', route('about-us'))->data('icon', 'undo');

                if (\Auth::check())
                {
                    $menu->add('Выйти', route('logout'));
                    if (\Auth::user()->isAdmin()) {
                        $menu->add('Админка', route('admin.index'));
                    }
                } else {
                    $menu->add('Войти', route('login'));
                }
            });
        }

        return $next($request);
    }
}
