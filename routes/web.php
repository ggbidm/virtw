<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('sitemap.xml', 'ArticlesController@sitemap')->name('sitemap');

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('ulogin', 'UloginController@login');

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'IsAdmin'],
    'namespace' => 'Admin',
    'as' => 'admin.',
], function () {
    Route::get('/', 'IndexController@index')->name('index');

    Route::get('/ajax/yad/visits', 'IndexController@yadVisits')->name('yad.visits');

    Route::resource('comments', 'CommentController');

    Route::get('/users', 'UserController@index')->name('users.index');
    Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::put('/users/{user}', 'UserController@update')->name('users.update');

    Route::get('/tasks', 'TasksController@index')->name('tasks');
    Route::get('/tasks/list', 'TasksController@taskList')->name('tasks.list');
    Route::post('/tasks/add', 'TasksController@create')->name('tasks.create');
    Route::get('/tasks/start/{id}', 'TasksController@start')->name('tasks.start');
    Route::get('/tasks/stop/{id}', 'TasksController@stop')->name('tasks.stop');
    Route::get('/tasks/finish/{id}', 'TasksController@finish')->name('tasks.finish');
    Route::get('/tasks/delete/{id}', 'TasksController@delete')->name('tasks.delete');
    Route::put('/tasks', 'TasksController@create')->name('tasks.update');


    Route::get('/keys', 'IndexController@keys')->name('keys');
    Route::resource('news', 'NewsController');
    Route::resource('articles', 'ArticlesController');
    Route::resource('categories', 'CategoriesController');

    Route::post('upload-image', 'AjaxController@loadImage')->name('admin.ajax.load-image');
});

Route::get('/', 'ArticlesController@index')->name('index');
Route::get('/{category}/{article}.html', 'ArticlesController@article')->name('article');
Route::post('/comment/store', 'CommentController@store')->name('comment.add');


Route::get('/poisk', 'ArticlesController@search')->name('search');
Route::get('/o-nas', 'ArticlesController@aboutUs')->name('about-us');

Route::get('/{category}', 'ArticlesController@category')->name('category');
