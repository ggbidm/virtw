<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        /*User::create([
            'password' => Hash::make('JH43*&jjhH'),
            'email' => 'nastya@virtw.ru',
            'name' => 'Nastya'
        ]);
        User::create([
            'password' => Hash::make('jkJh33h(&H'),
            'email' => 'sergey@virtw.ru',
            'name' => 'Sergey'
        ]);*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
